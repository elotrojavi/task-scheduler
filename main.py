from prettytable import PrettyTable, MARKDOWN
from jinja2 import Environment, PackageLoader, select_autoescape
from math import floor

tasks = [
    {
        'name': 'Salón',
        'done_every_x_weeks': 2,
    },
    {
        'name': 'Cocina',
        'done_every_x_weeks': 1,
    },
    {
        'name': 'Baño G+P',
        'done_every_x_weeks': 2,
        'excludes': ['Baño G']
    },
    {
        'name': 'Baño G',
        'done_every_x_weeks': 1,
    },
]

flatmates = [
    {
        'name': 'Mei',
        'extra_rests': 12
    },
    {
        'name': 'Éoj'
    },
    {
        'name': 'Lau'
    },
]

EMPTY = '-'
WEEKS = 52


def main():
    schedule = generate_empty_schedule()
    tasks_by_flatmate = generate_empty_tasks_by_flatmate()
    pending_tasks = []
    for week in range(WEEKS):
        week_tasks = get_tasks(week, pending_tasks)
        available_flatmates = get_available_flatmates(week)
        sorted_flatmate_priorities = get_sorted_flatmate_priority_by_task(week_tasks, tasks_by_flatmate)
        for flatmate in sorted_flatmate_priorities:
            if flatmates[flatmate[0]]['name'] in available_flatmates and flatmate[2] not in schedule[week]:
                tasks_by_flatmate[flatmate[0]].append(flatmate[2])
                schedule[week][flatmate[0]] = flatmate[2]
                available_flatmates.remove(flatmates[flatmate[0]]['name'])
        pending_tasks = [task for task in week_tasks if task not in schedule[week]]
    print_schedule_html(schedule)
    # print_schedule(schedule)
    # print_statistics(schedule)
    check_valid(schedule)
    find_loop(schedule)


def generate_empty_schedule():
    return [[EMPTY]*len(flatmates) for i in range(WEEKS)]


def generate_empty_tasks_by_flatmate():
    return [[EMPTY]*len(flatmates) for i in range(len(flatmates))]


def get_tasks(week, pending_tasks):
    week_tasks = [pending_task for pending_task in pending_tasks]
    excluded_tasks = []
    for task in tasks:
        if week % task['done_every_x_weeks'] == 0:
            week_tasks.append(task['name'])
            if 'excludes' in task:
                for excluded_task in task['excludes']:
                    excluded_tasks.append(excluded_task)
    return [task for task in week_tasks if task not in excluded_tasks]


def get_available_flatmates(week):
    available_flatmates = []
    for flatmate in flatmates:
        if 'extra_rests' not in flatmate or (flatmate['extra_rests'] == 0 or week % floor(WEEKS/flatmate['extra_rests']) != floor(WEEKS/flatmate['extra_rests'])-1):
            available_flatmates.append(flatmate['name'])
    return available_flatmates


def get_sorted_flatmate_priority_by_task(week_tasks, tasks_by_flatmate):
    flatmate_priority_by_task = get_flatmate_priorities(week_tasks, tasks_by_flatmate)
    flatmate_priorities = flatten_flatmate_priorities(flatmate_priority_by_task)
    sorted_flatmate_priorities = sort_flatmate_priorities(flatmate_priorities)
    return sorted_flatmate_priorities


def sort_flatmate_priorities(unsorted_priorities):
    return sorted(unsorted_priorities, key=lambda tup: (tup[1], -sum(tup[3:])))


def flatten_flatmate_priorities(flatmate_priority_by_task):
    return [flatmate for fp in flatmate_priority_by_task for flatmate in fp]


def get_flatmate_priorities(week_tasks, tasks_by_flatmate):
    flatmate_priority_by_task = []
    for task in week_tasks:
        flatmate_priority_by_task.append(get_flatmate_priority(task, tasks_by_flatmate, week_tasks))
    return flatmate_priority_by_task


def get_flatmate_priority(task, tasks_by_flatmate, week_tasks):
    task_iterations_by_flatmate = []
    for i, flatmate_tasks in enumerate(tasks_by_flatmate):
        flatmate_priority = [i, flatmate_tasks.count(task), task]
        for t in week_tasks:
            if t != task:
                flatmate_priority.append(flatmate_tasks.count(t))
        task_iterations_by_flatmate.append(flatmate_priority)
    return task_iterations_by_flatmate


def compute_rests(week, schedule):
    rests_by_flatmate = [0 for _ in flatmates]
    for i in range(len(flatmates)):
        for j in range(week+1):
            if (schedule[j][i] == EMPTY):
                rests_by_flatmate[i] += 1
    return rests_by_flatmate


def print_schedule(schedule):
    pretty_schedule = PrettyTable()
    pretty_schedule.align = 'l'
    pretty_schedule.set_style(MARKDOWN)
    header = [flatmate['name'] for flatmate in reversed(flatmates)]
    header.insert(0, 'Semana')
    pretty_schedule.field_names = header
    for i in range(WEEKS):
        row = schedule[i].copy()
        row.reverse()
        row.insert(0, i+1)
        pretty_schedule.add_row(row)
    print(pretty_schedule)


def print_schedule_html(schedule):
    env = Environment(
        loader=PackageLoader("main"),
        autoescape=select_autoescape()
    )
    header = [flatmate['name'] for flatmate in reversed(flatmates)]
    header.insert(0, '')
    template = env.get_template('schedule.html.j2')
    print(template.render(schedule=reverse_schedule_columns(schedule), header=header))

def reverse_schedule_columns(schedule):
    reversed_schedule = []
    for week in schedule:
        reversed_schedule.append(reversed(week.copy()))
    return reversed_schedule

def print_statistics(schedule):
    for i in range(3):
        tasks_done = []
        for j in range(WEEKS):
            tasks_done.append(schedule[j][i])
        print(f'{flatmates[i]['name']}: {tasks[0]['name']} {tasks_done.count(tasks[0]['name'])} veces, {tasks[1]['name']} {tasks_done.count(tasks[1]['name'])} veces, {tasks[2]['name']} {tasks_done.count(tasks[2]['name'])} veces, {tasks[3]['name']} {tasks_done.count(tasks[3]['name'])} veces. Total: {WEEKS-tasks_done.count(EMPTY)}, descansos: {tasks_done.count(EMPTY)} ')


def check_valid(schedule):
    for task in tasks:
        for week in range(WEEKS):
            if week % task['done_every_x_weeks'] == 0:
                assert task['name'] in schedule[week] or task['name'] in schedule[week+1], f'La semana {week+1} no tiene la tarea {task['name']}'


def find_loop(schedule):
    consecutive_matches = 0
    loop_ref = 0
    loop_start = None
    for i in range(1, WEEKS):
        if schedule[loop_ref] == schedule[i]:
            if loop_ref == loop_start:
                print(f'Bucle encontrado de semana {loop_start+1} a {loop_start + consecutive_matches+1}')
                break
            loop_ref += 1
            consecutive_matches += 1
            if consecutive_matches == 1:
                loop_start = i
        elif loop_start:
            consecutive_matches = 0
            loop_ref = 0
            loop_start = None
    if consecutive_matches > 0 and loop_ref != loop_start:
        print(f'Bucle parcial desde {loop_start+1} hasta {loop_start+consecutive_matches}')


if __name__ == "__main__":
    main()
